import com.ComApp;
import com.epam.ComEpamApp;
import net.NetApp;
import net.epam.NetEpamApp;
//https://www.journaldev.com/7128/log4j2-example-tutorial-configuration-levels-appenders
public class Main {
	public static void main(String [] args){
		new ComApp();
		new ComEpamApp();
		new NetApp();
		new NetEpamApp();
	}
}